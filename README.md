fosdem-volunteers
=================

Volunteers management system for conferences, originally written for FOSDEM.

SETUP
=====

```
# Setup and activate virtual environment
virtualenv .venv
source .venv/bin/activate

# Install dependencies
pip2 install -r requirements.txt

# Run migrations
python2 manage.py makemigrations
python2 manage.py migrate

# Run webapp
python manage.py runserver
```
