# django stuff
Django
gunicorn
dj-static
django-extensions

# database stuff
psycopg2
dj_database_url

# userena stuff
xhtml2pdf
vobject
reportlab
html5lib
easy-thumbnails
python-dateutil
django-userena-ce
django-guardian
